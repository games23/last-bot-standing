﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Systems
{
    public class HealthSystem : MonoBehaviour, BotSystem
    {
        public ParticleSystem Fire;

        private SystemProvider systemProvider;

        public void Initialize(SystemProvider systemProvider) => 
            this.systemProvider = systemProvider;

        public void Disable()
        {
            Fire.Play();
            enabled = false;
        }

        public void ReceiveDamage() => OnDestroy();

        void OnDestroy() => systemProvider.ShutDown();
    }
}
