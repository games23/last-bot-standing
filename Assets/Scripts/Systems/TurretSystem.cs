﻿using System.Collections;
using Assets.Scripts.Weapons;
using UnityEngine;

namespace Assets.Scripts.Systems
{
    public class TurretSystem : MonoBehaviour, BotSystem
    {
        public CannonShot CannonShotPrefab;
        public float RotationSpeed;
        public float ShotForce;
        public float FireRate;
        public ParticleSystem ShotFireVfx;
        public Transform FirePosition;
        public Animator Animator;

        private bool canFire = true;
        
        public void Fire()
        {
            if (canFire && enabled)
            {
                ShotFireVfx.Play();
                Animator.SetTrigger("Fire");
                var shot = Instantiate(CannonShotPrefab, FirePosition.position, FirePosition.rotation);
                shot.Initialize(ShotForce);

                canFire = false;
                StartCoroutine(Reload());
            }
        }

        public void Disable() => enabled = false;

        private IEnumerator Reload()
        {
            yield return new WaitForSeconds(FireRate);
            canFire = true;
        }

        public void Initialize(SystemProvider systemProvider)
        {
            
        }

        public void LookAt(Transform activeTarget)
        {
            var relativePosition = transform.InverseTransformPoint(activeTarget.position);
            var rotationDirectionFactor = relativePosition.x < 0 ? -1 : 1;
            
            transform.Rotate(transform.up * RotationSpeed * rotationDirectionFactor);
            //transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
        }

        public bool AimedAt(Transform activeTarget)
        {
            var relativePosition = transform.InverseTransformPoint(activeTarget.position);
            return Mathf.Abs(relativePosition.x) < 3f;
        }
    }
}
