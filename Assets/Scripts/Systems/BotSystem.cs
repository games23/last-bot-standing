﻿namespace Assets.Scripts.Systems
{
    public interface BotSystem
    {
        void Initialize(SystemProvider systemProvider);
        void Disable();
    }
}
