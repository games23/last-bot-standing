﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Bots;
using UnityEngine;

namespace Assets.Scripts.Directors
{
    public class TargetProvider
    {
        private List<BotCore> bots;

        public TargetProvider(IEnumerable<BotCore> bots) => 
            this.bots = bots.ToList();

        public BotCore ClosestEnemyInRange(BotCore sourceBot, float range) => 
            bots
            .Where(bot => bot != sourceBot)
            .Select(bot => new
                {bot, distance = Vector3.Distance(bot.transform.position, sourceBot.transform.position)})
            .Where(botDistance => botDistance.distance < range)
            .OrderByDescending(botDistance => botDistance.distance)
            .First()?.bot;

        public void OnBotDestroyed(BotCore destroyedBot)
        {
            bots.Remove(destroyedBot);
            foreach (var bot in bots)
                bot.RefreshTarget();
        }

        public bool AnyBotInRange(BotCore sourceBot, float range) =>
            bots
                .Where(bot => bot != sourceBot)
                .Any(bot => Vector3.Distance(bot.transform.position, sourceBot.transform.position) < range);

        public IEnumerable<BotCore> AllBotsInRange(BotCore sourceBot, float range) =>
            bots
                .Where(bot => bot != sourceBot)
                .Where(bot => Vector3.Distance(bot.transform.position, sourceBot.transform.position) < range);

        public bool StillAlive(BotCore targetBot) => 
            bots.Contains(targetBot);
    }
}