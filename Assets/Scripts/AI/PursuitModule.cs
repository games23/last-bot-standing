﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Systems;
using UnityEditor.Build;
using UnityEngine;

namespace Assets.Scripts.AI
{
    public class PursuitModule : MonoBehaviour, AIModule
    {
        private MovementSystem movementSystem;
        private RadarSystem radarSystem;
        private Transform activeTarget;
        private PatrolModule patrolModule;

        public void Initialize(AIModuleProvider aiModuleProvider, SystemProvider systemProvider)
        {
            this.movementSystem = systemProvider.Find<MovementSystem>();
            this.radarSystem = systemProvider.Find<RadarSystem>();
            this.patrolModule = aiModuleProvider.Find<PatrolModule>();
        }

        public void StartPursuit(Transform target)
        {
            activeTarget = target;
            movementSystem.MoveTo(target.position, ContinuePursuit);
        }

        private void ContinuePursuit()
        {
            if(radarSystem.IsStillAlive(activeTarget))
                movementSystem.MoveTo(activeTarget.position, ContinuePursuit);
            else
            {
                patrolModule.StartPatrol();
                Disable();
            }
        }


        public void Disable()
        {
            activeTarget = null;
            StopAllCoroutines();
        }
    }
}
