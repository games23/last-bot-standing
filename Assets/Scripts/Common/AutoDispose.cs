﻿using UnityEngine;

namespace Assets.Scripts.Common
{
    public class AutoDispose : MonoBehaviour
    {
        public float Lifetime;

        void Start() => Destroy(gameObject, Lifetime);
    }
}
